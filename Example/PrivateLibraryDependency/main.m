//
//  main.m
//  PrivateLibraryDependency
//
//  Created by feimengchang on 05/27/2017.
//  Copyright (c) 2017 feimengchang. All rights reserved.
//

@import UIKit;
#import "PZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PZAppDelegate class]));
    }
}
