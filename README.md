# PrivateLibraryDependency

[![CI Status](http://img.shields.io/travis/feimengchang/PrivateLibraryDependency.svg?style=flat)](https://travis-ci.org/feimengchang/PrivateLibraryDependency)
[![Version](https://img.shields.io/cocoapods/v/PrivateLibraryDependency.svg?style=flat)](http://cocoapods.org/pods/PrivateLibraryDependency)
[![License](https://img.shields.io/cocoapods/l/PrivateLibraryDependency.svg?style=flat)](http://cocoapods.org/pods/PrivateLibraryDependency)
[![Platform](https://img.shields.io/cocoapods/p/PrivateLibraryDependency.svg?style=flat)](http://cocoapods.org/pods/PrivateLibraryDependency)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PrivateLibraryDependency is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PrivateLibraryDependency"
```

## Author

feimengchang, zlanchun@icloud.com

## License

PrivateLibraryDependency is available under the MIT license. See the LICENSE file for more info.
